require('dotenv').config();
var express = require('express');
var app = express();
var server = require('http').Server(app);
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var MongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectId;
var io = require('socket.io')(server);
var session = require('express-session');
var flash = require('connect-flash');
var bodyParser = require('body-parser');
var bcrypt = require('bcrypt-nodejs');
var saltRounds = 10;
var request = require('request');
var https = require('https');


server.listen(8000, function () {
	console.log('Example app listening on port 8000!');
});

app.use(express.static(__dirname + '/public'));
app.use(session({
	secret: process.env.SESSION,
	resave: true,
	saveUninitialized: true
}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(flash());
app.use(passport.initialize());
app.use(passport.session());

function DbConnection() {
	return new Promise((resolve, reject) => {
		MongoClient.connect(process.env.DB_PATH, 
			(err, db) => {
 				err ? reject(err) : resolve(db);
			}
		);
	});
};

var isAuthenticated = function (req, res, next) {
	if (req.isAuthenticated())
		return next();
	else
		console.log('not authenticated');
		res.redirect('/');
};

passport.use('register', 
	new LocalStrategy(
		function (username, password, done) {
			bcrypt.hash(password, saltRounds, function (err, hash) {
				DbConnection().then(db => {
					db.collection('users').insert({
						'username': username,
						'password': hash,
						'charges': [],
						'balance': 0,
						'files': [],
						'fileHistory': [],
						'storageUsed': 0
					}, function(err, user) {
						return done(null, user);
					});
				})
			});
		}
	)
);

passport.use('login',
	new LocalStrategy(
		function (username, password, done) {
			DbConnection().then(db => {
			    db.collection('users').findOne({ 'username': username }, function (err, user) {
					if (err) { return done(err); }
					if (!user) { return done(null, false); }
					bcrypt.compare(password, user.password, function(err, res) {
						if (res = false) { return done(null, false); }
						return done(null, user);
					});
					// if (user.password != password) { return done(null, false); }
					// return done(null, user);
			    });
		  	});
		}
	)
);

passport.serializeUser(function (user, done) {
	done(null, user);
});

passport.deserializeUser(function (user, done) {
	done(null, user);
});

app.post('/register', 
	passport.authenticate('register'), 
	function (req, res) {
		console.log(req.session);
		res.redirect('/');
	}
);

app.post('/login', 
	passport.authenticate('login'), 
	function (req, res) {
		console.log(req.session);
		res.redirect('/home');
	}
);

app.post('/logout', 
	function (req, res) {
		console.log(req.session);
		req.logout();
		console.log(req.session);
		res.redirect('/');
	}
);

app.get('/', function (req, res) {
	res.sendFile(__dirname + '/index.html');	
});

app.get('/home', function (req, res) {
	res.sendFile(__dirname + '/home.html');	
});

app.post('/payments',
	function(req, res) {
		console.log(req.body);
		console.log(JSON.parse(req.body));
		res.status(200);
		res.send();
	}
)

app.post('/createCharge',
	function(req, res) {
		request.post(
			{
				url: 'https://api.commerce.coinbase.com/charges', 
				headers: {
					'Content-Type': 'application/json',
					'X-CC-Api-Key': process.env.COINBASE_APIKEY,
					'X-CC-Version': '2018-03-22',
				},
				form: {
					"name":"Sia Storage",
					"description":"Prepay for storage",
					"pricing_type":"no_price"
				}
			}, 
			function(error, response, body) {
				console.log(JSON.parse(body))
				res.redirect(JSON.parse(body).data.hosted_url)
			}
		)
	}
)

app.post('/wallet', 
	function (req, res) {
		request({url: 'http://localhost:9980/wallet', headers: {'User-Agent': 'Sia-Agent'}}, function(error, response, body) {
			console.log(body)
		});
	}
);
