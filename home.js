require('dotenv').config();
var request = require('request');


app.post('/createCharge',
	function(req, res), {
		request(
			{
				url: 'https://api-commerce.coinbase.com/charges/', 
				headers: {
					'User-Agent': 'Sia-Agent',
					'X-CC-Api-Key': process.env.COINBASE_APIKEY,
					'X-CC-Version': 2018-03-22
				},
				data: {
					'name': 'Sia Storage',
					'description': 'Prepay for storage',
					'pricing_type': 'no_price'
				}
			}, 
			function(error, response, body) {
				console.log(body)
			}
		)
	}
)
// request({url: 'http://localhost:9980/wallet', headers: {'User-Agent': 'Sia-Agent'}}, function(error, response, body) {
// 	console.log(body)
// })

// app.post('/wallet', 
// 	function (req, res) {
// 		request({url: 'http://localhost:9980/wallet', headers: {'User-Agent': 'Sia-Agent'}}, function(error, response, body) {
// 			console.log(body)
// 		});
// 	}
// );